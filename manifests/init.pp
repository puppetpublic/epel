#
# Configuration specific to using packages from EPEL repo. This class is
# meant to cater directly to RedHat hosts, though CentOS and Oracle Linux
# hosts may be able to make good use of this as well.

class epel {
    case $::lsbmajdistrelease {
        # no-op for EL 3/4
        '3', '4': {
            fail 'epel not supported on this platform'
        }
        # EL 5/6/7
        default: {
            if ($::lsbmajdistrelease == '7' and $::architecture != 'x86_64') {
                fail 'epel not available for EL7 i386'
            }
            include epel::rpmkey
            file { '/etc/yum.repos.d/epel.repo':
                source => "puppet:///modules/epel/yum.repos.d/epel-EL$::lsbmajdistrelease.repo";
            }
        }
    }
}


# rpmkey for epel
class epel::rpmkey {
    case $::lsbmajdistrelease {
        '5': {
            base::rpm::import {
                epel-rpmkey:
                url       => 'http://yum.stanford.edu/RPM-GPG-KEY-EPEL',
                signature => 'gpg-pubkey-217521f6-45e8a532',
            }
        }
        '6': {
            base::rpm::import {
                epel-rpmkey:
                    url       => 'http://yum.stanford.edu/RPM-GPG-KEY-EPEL-6',
                    signature => 'gpg-pubkey-0608b895-4bd22942',
            }
        }
        '7': {
            base::rpm::import {
                epel-rpmkey:
                    url       => 'http://yum.stanford.edu/RPM-GPG-KEY-EPEL-7',
                    signature => 'gpg-pubkey-352c64e5-52ae6884',
            }
        }

    }
}

